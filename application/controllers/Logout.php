<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Logout extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->database();
        $this->load->library('session');
        $this->load->helper('download');
        $this->load->model('Model_siswa');
        $this->load->model('Model_spk');
    }
    public function index() {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role');
        session_destroy();
        redirect();
    }
}