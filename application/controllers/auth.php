<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->database();
		$this->load->library('session');
		//$this->load->library('encrypt');
		$this->load->helper('security');
	}
	public function index() {
		$this->load->view('auth/Login');
	}

	public function cek_login() {
		$password = $this->input->post('katasandi');
		//$pass =  do_hash($password, 'md5');
		$data = array('namauser' => $this->input->post('namauser', TRUE),
						//'password' => $pass
			'katasandi' => $password
			);
		$this->load->model('Model_user'); // load model_user
		$hasil = $this->Model_user->cek_user($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['id'] = $sess->loginID;
				$sess_data['namapengguna'] = $sess->namapengguna;
				$sess_data['namauser'] = $sess->namauser;
				$sess_data['role'] = $sess->role;
				$this->session->set_userdata($sess_data);
			}

			if ($sess_data['role'] == 'admin') {
					echo "<script>alert('Selamat datang, ".$sess_data['namapengguna']."') ; window.location.href = '../Admin/dashboard'</script>";
			}else{
					echo "<script>alert('Selamat datang, ".$sess_data['namapengguna']."') ; window.location.href = '../Orangtua/dashboard'</script>";
			}
//echo '<script>alert("selamat datang, '.$sess_data['namapengguna'].'");</script>'; redirect('Dashboard', 'refresh');

			//redirect('Dashboard');
		}
		else {
			echo "<script>alert('Login Gagal. Cek username dan password anda.');history.go(-1);</script>";
		}
	}

}

?>