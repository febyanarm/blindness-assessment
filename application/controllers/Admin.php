<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Admin extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->database();
        $this->load->library('session');
        $this->load->helper('download');
        if (($this->session->userdata('namauser')=="")&&($this->session->userdata('role')!="admin")) {
            $this->session->unset_userdata('namauser');
            session_destroy();

            echo"Anda Belum Login";
            redirect('Welcome');
        }
        $this->load->model('Model_siswa');
        $this->load->model('Model_spk');
    }
    public function dashboard()
    {
        $data['hal'] = 'dashboard';
        $data1['hitung_jkL'] = $this->Model_siswa->hitung_jkL();
        $data1['hitung_jkP'] = $this->Model_siswa->hitung_jkP();
        $data1['jml_siswa'] = $this->Model_siswa->jml_siswa();
        $data1['data_siswa'] = $this->Model_siswa->data_siswa();
        $data['content'] = $this->load->view('admin/dashboard', $data1, TRUE);
        $this->load->view('skin/index', $data);
    }
    public function pendaftaran()
    {
        $data['hal'] = 'pendaftaran';
        $data1['data_siswa'] = $this->Model_siswa->data_siswa();
        $data['content'] = $this->load->view('admin/pendaftaran', $data1, TRUE);
        $this->load->view('skin/index', $data);
    }
    public function asesmen()
    {
        $data['hal'] = 'asesmen';
        $data1['ambildata'] = $this->Model_spk->ambildata();
        $data['content'] = $this->load->view('admin/asesmen', $data1, TRUE);
        $this->load->view('skin/index', $data);
    }
    function dftr_manual(){
        $this->Model_siswa->dftr_manual(); 
        echo "<script>alert('Data berhasil ditambahkan!');
        window.location.href='".base_url()."Admin/pendaftaran';
        </script>";
    }
 public function daftarkan(){
        $fileName = time().$_FILES['file']['name'];
         
        $config['upload_path'] = FCPATH.'assets/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
        $inputFileName = $this->upload->data('full_path');
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                // membuat ID atau NIS
                $tahun = date("Ymd");
                $today = date("Y-m-d");
                $kode = 'MAT';
                $query = $this->db->query("SELECT MAX(sisID) as max_id FROM data_siswa where sisTgl = '$today'"); 
                $row = $query->row_array();
                $max_id = $row['max_id']; 
                $max_id1 =(int) substr($max_id,11,3);
                $peng_ID = $max_id1 +1;
                $maxres_ID = $tahun.sprintf("%03s",$peng_ID);

                //collecting data                                
                 $data = array(
                    "sisID"=> $maxres_ID,
                    "sisNama"=> $rowData[1][1],
                    "sisJK"=> $rowData[1][2],
                    "sisLahir"=> $rowData[1][3],
                    "sisAgama"=> $rowData[1][4],
                    "sisAyah"=> $rowData[1][5],
                    "sisIbu"=> $rowData[1][6],
                    "sisTlp"=> $rowData[1][7],
                    "sisEmail"=> $rowData[1][8]
                );               
                $namauser = trim($rowData[1][1], " ");  
                $un = substr($namauser,0,5);               
                 $data_login = array(
                    "loginID"=> $maxres_ID,
                    "namauser"=> $un,
                    "katasandi"=> $rowData[1][7],
                    "namapengguna"=> $rowData[1][1],
                    "role"=> 'orangtua'
                );

                if($rowData[1][1] != NULL){
                    $insert = $this->db->insert("data_siswa",$data);
                    $input = $this->db->insert("login",$data_login);
                    echo "<script>alert('Data berhasil ditambahkan!');
                    window.location.href='".base_url()."Admin/pendaftaran';
                    </script>";
                }else{
                echo "<script>alert('Gagal Menambahkan data. Data Kosong!');
                    window.location.href='".base_url()."Admin/pendaftaran';
                </script>";
                }
                //sesuaikan nama dengan nama tabel
            }

    }

    public function download($tautan){
        $path = "assets/template/";
        force_download($path.$tautan,NULL);
    }   

    
    public function logout() {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role');
        session_destroy();
        redirect();
    }
}