<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
function __construct(){
		parent::__construct();
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->helper('url');
		$this->load->library('session');
		if ($this->session->userdata('namauser')=="") {
			$this->session->unset_userdata('namauser');
			session_destroy();

			echo"Anda Belum Login";
			redirect('Welcome');
		}
		$this->load->database();
		$this->load->model('Model_spk');
		$this->load->model('Model_siswa');
		$this->load->library('pagination');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['namauser'] = $this->session->userdata('namauser');
		$data['namapengguna'] = $this->session->userdata('namapengguna');
		$this->load->view('Dashboard',$data);
	}
	public function logout() {
		$this->session->unset_userdata('namauser');
		//$this->session->unset_userdata('');
		session_destroy();
		redirect('Welcome');
	}
	function skala_jarak($str){
		$arr = preg_split('/(?<=[0-9])(\D+)/i',$str);   
		//buat penyebutnya menjadi pecahan 10, ambil angka pembaginya, bagi pembilang dan penyebut dengan angka tsb                                          
                                                                                                                                                             
		$pembilang = $arr[0];
		$penyebut = $arr[1];
			
		$a_pembagi = 10/$penyebut;
				//validasi hasil penyebut dibagi a_pembagi == 10
		$cd= $penyebut*$a_pembagi;
				//sederhanakan pembilang
		$sdr_pemb = $pembilang*$a_pembagi;
				//echo $sdr_peny;

		if ($pembilang != 0) {
				//klaster tidak penting - sangat penting
				$klaster = $pembilang/$penyebut;
				
		}else{
				$klaster = 0;
		}
		var_dump($klaster);
		if( filter_var($sdr_pemb, FILTER_VALIDATE_INT) ){
				$skala = intval($sdr_pemb);
				}else {
					$skala = intval($sdr_pemb)+1;
				}
		return array($skala,$klaster);

	}
	function priority_perception($perception){
		$skala_jarak = $this->skala_jarak($perception);
		$skala = $skala_jarak[0];
		$klaster = $skala_jarak[1];
		var_dump($skala_jarak[0]);
		var_dump($skala_jarak[1]);
		//$perception = $rightanswer / $question;
		if($klaster > 0 && $klaster < 0.6){
			$penting = 'p';
			$l_perception = 'No Light Perception';
		}elseif ($klaster == 0) {
			$penting = 'sp';
			$l_perception = 'Total Blindness';
		}elseif ($klaster >= 0.6) {
			$penting = 'tp';
			$l_perception = 'Light Perception';
		}

		return array($penting,$skala);
	}
	function priority_visus($visus){
		$skala_jarak = $this->skala_jarak($visus);
		$skala = $skala_jarak[0];
		$klaster = $skala_jarak[1];
		//$visus = $d / $D;
		if($klaster < 0.025){
			$penting = 'sp';
			$l_visus = 'Blind / Almost Blind';
		}elseif ($klaster >= 0.025 && $klaster < 0.1) {
			$penting = 'p';
			$l_visus = 'Real Low Vision';
		}elseif ($klaster >= 0.1) {
			$penting = 'tp';
			$l_visus = 'Low Vision / Almost Normal';
		}
		return array($penting,$skala);
	}
	function priority_age($age){
		if($age > 20){
			$age = 20;
		}else{
			$age = intval($age);
		}
		$pembilang = $age;
		$penyebut = 20;
		$a_pembagi = 10/$penyebut;
		//validasi hasil penyebut dibagi a_pembagi == 10
		$cd= $penyebut*$a_pembagi;
		//sederhanakan pembilang
		$sdr_pemb = $pembilang*$a_pembagi;
		//echo $sdr_peny;

		if( filter_var($sdr_pemb, FILTER_VALIDATE_INT) ){
		$skala = intval($sdr_pemb);
		}else {
			$skala = intval($sdr_pemb)+1;
		}
		//klaster tidak penting - sangat penting
		$klaster = $pembilang/$penyebut;

		if($klaster < 0.4){
			$penting = 'sp';
			$l_age = 'Pre-Natal - toddler';
		}elseif ($klaster >= 0.4 && $klaster < 0.73) {
			$penting = 'p';
			$l_age = 'Kid';
		}elseif ($klaster >= 0.73) {
			$penting = 'tp';
			$l_age = 'Teenager';
		}
		return array($penting,$skala);
	}
	function priority_braile($b){
		$skala_jarak = $this->skala_jarak($b);
		$skala = $skala_jarak[0];
		$klaster = $skala_jarak[1];
		
		if($klaster < 0.33){
			$penting = 'tp';
			$l_braile = 'Visual';
		}elseif ($klaster >= 0.33 && $klaster < 0.66) {
			$penting = 'p';
			$l_braile = 'Middle ( Braile + Visual )';
		}elseif ($klaster >= 0.66) {
			$penting = 'sp';
			$l_braile = 'Braile';
		}
		return array($penting,$skala);
	}
	function kpk($a,$b){
		if ($a == $b) {
			$hasil = ($a+$b)/2;
		}elseif ($a < $b) {
			$hasil = $b;
		}elseif ($a > $b) {
			$hasil = $a;
		}
		return $hasil;
	}
	function perkalian_matriks($matriks_a, $matriks_b) {
	$a_i = sizeof($matriks_a);
	$a_j = sizeof($matriks_a[0]);
	
	$b_i = sizeof($matriks_b);
	$b_j = sizeof($matriks_b[0]);
	
	$hasil = array();
	for ($i=0; $i<$a_i; $i++) {
		for ($j=0; $j<$b_j; $j++) {
			$temp = 0;
			for ($k=0; $k<$b_i; $k++) {
				$temp += $matriks_a[$i][$k] * $matriks_b[$k][$j];
			}
			$hasil[$i][$j] = $temp;
		}
	}
		return $hasil;
	}
	function skala_prioritas($eigen1,$eigen2,$eigen3,$eigen4){
		$numbers = array($eigen1,$eigen2,$eigen3,$eigen4);
		rsort($numbers);

		$arrlength = count($numbers);
		$priority = array();
		for($x = 0; $x < $arrlength; $x++) {
		    $priority[] = $numbers[$x];
		}
		return $priority;
	}
	function matriks_pairwise($vb,$mb,$pb,$mv,$pv,$pm){
		$pp = 1;
		$vv = 1;
		$mm = 1;
		$bb = 1;
		$bv = 1/$vb;
		$bm = 1/$mb;
		$bp = 1/$pb;
		$vm = 1/$mv;
		$vp = 1/$pv;
		$mp = 1/$pm;
		//var_dump($bv);

//persamaan penyebut
		$persamaan1 = $this->kpk($bm,$vm);
		$persamaan2 = $this->kpk($bp,$vp);
		$persamaan2a = $this->kpk($persamaan2,$mp);

//penyederhanaan pairwise
		$col1 = $bb+$vb+$mb+$pb;
		$col2 = ($bv*$vb)+$vv+$mv+$pv;
		$col3 = ($bm*$persamaan1)+($vm*$persamaan1)+$mm+$pm;
		$col4 = ($bp*$persamaan2a)+($vp*$persamaan2a)+($mp*$persamaan2a)+$pp;

//dibulatkan (dibagi dengan hasil penjumlahan sehingga hasil = 1)
		//column 1
		$bb_1 = $bb / $col1;
		$vb_1 = $vb / $col1;
		$mb_1 = $mb / $col1;
		$pb_1 = $pb / $col1;
		
		//column 2
		$bv_1 = ($bv*$vb) / $col2;
		$vv_1 = $vv / $col2;
		$mv_1 = $mv / $col2;
		$pv_1 = $pv / $col2;

		//column 3
		$bm_1 = ($bm*$persamaan1) / $col3;
		$vm_1 = ($vm*$persamaan1) / $col3;
		$mm_1 = $mm / $col3;
		$pm_1 = $pm / $col3;

		//column 4
		$bp_1 = ($bp*$persamaan2a) / $col4;
		$vp_1 = ($vp*$persamaan2a) / $col4;
		$mp_1 = ($mp*$persamaan2a) / $col4;
		$bb_1 = $bb / $col4;

//jumlahkan per baris
		
		$baris1 = $bb_1+$bv_1+$bm_1+$bp_1;
		$baris2 = $vb_1+$vv_1+$vm_1+$vp_1;		
		$baris3 = $mb_1+$mv_1+$mm_1+$mp_1;
		$baris4 = $pb_1+$pv_1+$pm_1+$bb_1;

		$ttl = $baris1+$baris2+$baris3+$baris4;

//dapatkan v eigen
		$eigen1 = $baris1 / $ttl;
		$eigen2 = $baris2 / $ttl;
		$eigen3 = $baris3 / $ttl;
		$eigen4 = $baris4 / $ttl;


		$a = array();
		$a[] = array($bb,$bv,$bm,$bp);
		$a[] = array($vb,$vv,$vm,$vp);
		$a[] = array($mb,$mv,$mm,$mp);
		$a[] = array($pb,$pv,$pm,$pp);

		$b = array();
		$b[] = array($eigen1);
		$b[] = array($eigen2);
		$b[] = array($eigen3);
		$b[] = array($eigen4);

		$aw = $this->perkalian_matriks($a,$b);

//// SKALA PRIORITAS ////
		$prioritas = $this->skala_prioritas($eigen1,$eigen2,$eigen3,$eigen4);
		$p_1 = $prioritas[0];
		$p_2 = $prioritas[1];
		$p_3 = $prioritas[2];
		$p_4 = $prioritas[3];

// aw : eigen
		$i1 = $aw[0][0] / $eigen1;
		$i1 = $aw[1][0] / $eigen2;
		$i1 = $aw[2][0] / $eigen3;
		$i1 = $aw[3][0] / $eigen4;

		$ttl_i = $i1+$i1+$i1+$i1;

// mencari lamda maks
		$lamda = $ttl_i / 4;

//mencari CI
		$ci = ($lamda - 4) / 3;

//mencari CR (dikarenakan jumlah n = 4 maka RI = 0,90)
		$cr = $ci / 0.90;

		return array($p_1,$p_2,$p_3,$p_4,$cr,$eigen1,$eigen2,$eigen3,$eigen4);
	}
	function skala_kepentingan($a,$b,$c,$d){
		//mencari kelas skala pembanding 1
		$kelas_c = 0;
		$kelas_d = 0;
			if ($c == 1 && $c==2) {
				$kelas_c = 1;
			}elseif ($c == 3 && $c==4) {
				$kelas_c = 2;
			}elseif ($c == 5 && $c==6) {
				$kelas_c = 3;
			}elseif ($c == 7 && $c==8) {
				$kelas_c = 4;
			}elseif ($c == 9 && $c==10) {
				$kelas_c = 5;
			}
//var_dump($kelas_c);
		//mencari kelas skala pembanding 2
			if ($d==1 && $d==2) {
				$kelas_d = 1;
			}elseif ($d==3 && $d==4) {
				$kelas_d = 2;
			}elseif ($d==5 && $d==6) {
				$kelas_d = 3;
			}elseif ($d==7 && $d==8) {
				$kelas_d = 4;
			}elseif ( $d==9 && $d==10) {
				$kelas_d = 5; 
			}

		$selisih = abs($kelas_c - $kelas_d);

		if($kelas_c < $kelas_d){
			$jarak = 1/$selisih;
		}else{
			$jarak = $selisih;
		}

		//mencari tipe skala kepentingan
		if($a == $b){
			$label = 'ragu';
			if($jarak == 1 || $jarak == 0){
				$jarak = 2;
			}elseif ($jarak == 2) {
				$jarak == 4;
			}elseif ($jarak == 3) {
				$jarak == 6;
			}elseif ($jarak == 4) {
				$jarak == 8;
			}
			return $jarak;
		}else{
			$label = 'lebih penting';
			if($jarak == 1 || $jarak == 0){
				$jarak = 3;
			}elseif ($jarak == 2) {
				$jarak == 5;
			}elseif ($jarak == 3) {
				$jarak == 7;
			}elseif ($jarak == 4) {
				$jarak == 9;
			}
			return $jarak;
		}
	}

    function generate(){
    	$this->upload();
                    echo "<script>alert('Data berhasil ditambahkan!');
                    window.location.href='".base_url()."Admin/asesmen';
                    </script>";
    }
    function manual_generate(){
    	$this->hitung();
                    echo "<script>alert('Data berhasil ditambahkan!');
                    window.location.href='".base_url()."Admin/asesmen';
                    </script>";
    }
    function hitung(){
    		// $c = $this->input->post('c');
    		// $q = $this->input->post('q');
    		// $perc = $c."/".$q;

    		// $sight = $this->input->post('sight');
    		// $norm = $this->input->post('norm');
    		// $vis = $sight."/".$norm;
			$perc = $this->input->post('perc');
			//var_dump($perc);
			$vis = $this->input->post('vis');
    		
    		$age = $this->input->post('age');
    		$usia = $age."/20";

    		$score = $this->input->post('braile')."/10";

                //cari nilai prioritas dulu
                 $visus = $this->priority_visus($vis);
                 $persepsi = $this->priority_perception($perc);
                 $moment = $this->priority_age($usia);
                 $braile = $this->priority_braile($score);

                 $vb = $this->skala_kepentingan($visus[0],$braile[0],$visus[1],$braile[1]);
                 $mb = $this->skala_kepentingan($moment[0],$braile[0],$moment[1],$braile[1]);
                 $pb = $this->skala_kepentingan($persepsi[0],$braile[0],$persepsi[1],$braile[1]);
                 $mv = $this->skala_kepentingan($moment[0],$visus[0],$moment[1],$visus[1]);
                 $pv = $this->skala_kepentingan($persepsi[0],$braile[0],$persepsi[1],$braile[1]);
                 $pm = $this->skala_kepentingan($persepsi[0],$moment[0],$persepsi[1],$moment[1]);

                 $pairwise = $this->matriks_pairwise($vb,$mb,$pb,$mv,$pv,$pm);

                 $gabungan = array();
                 $gabungan[] = array(0.61,0.54,0.57,0.16);
                 $gabungan[] = array(0.30,0.30,0.29,0.22);
                 $gabungan[] = array(0.26,0.16,0.14,0.62);

                 $v_eigen = array();
                 $v_eigen[] = array($pairwise[5]);
                 $v_eigen[] = array($pairwise[6]);
                 $v_eigen[] = array($pairwise[7]);
                 $v_eigen[] = array($pairwise[8]);

                 $spk = $this->perkalian_matriks($gabungan,$v_eigen);

                 $urut = max($spk[0][0],$spk[1][0],$spk[2][0]);
                 if ($urut == $spk[0][0]) {
                 	$kelas = 'Total Blindness';
                 }elseif ($urut == $spk[1][0]) {
                 	$kelas = 'Sedang';
                 }elseif ($urut == $spk[2][0]) {
                 	$kelas = 'Low Vision';
                 }

				$datas = array(
                    //"idimport"=> $rowData[0][0],
                    "nama_siswa"=> $this->input->post('name'),
                    "persepsi"=> $perc,
                    "visus"=> $vis,
                    "moment"=> $usia,
                    "braile"=> $score,
                    "tkt_disb" => $kelas,
                    "tgl_upload"=> date("Y-m-d H:i:s")
                );
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("hasil_asesmen",$datas);
    }

	function upload(){
        $fileName = time().$_FILES['file']['name'];
         
        $config['upload_path'] = FCPATH.'assets/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
        $inputFileName = $this->upload->data('full_path');
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
            
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 5; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                //Sesuaikan sama nama kolom tabel di database                                
                 $data = array(
                    //"idimport"=> $rowData[0][0],
                    "sisID"=> $rowData[0][0],
                    "sisNama"=> $rowData[0][1],
                    "persepsi"=> $rowData[0][2],
                    "visus"=> $rowData[0][3],
                    "moment"=> $rowData[0][4],
                    "braile"=> $rowData[0][5]
                );


				var_dump($rowData[0][2]);
				var_dump($rowData[0][3]);
                 //tentukan nilai perbandingannya dulu
                 //persepsi standarnya jawaban benar / pertanyaan (std: percobaan 10x)
                 //$persepsi_nilai = $data['persepsi'];
                 //$persepsi_siswa = 1/$persepsi_nilai;

                 //visus satuan standarnya


                //cari nilai prioritas dulu
                 $persepsi = $this->priority_perception($data['persepsi']);
                 $visus = $this->priority_visus($data['visus']);
                 $moment = $this->priority_age($data['moment']);
                 $braile = $this->priority_braile($data['braile']);

                 $vb = $this->skala_kepentingan($visus[0],$braile[0],$visus[1],$braile[1]);
                 $mb = $this->skala_kepentingan($moment[0],$braile[0],$moment[1],$braile[1]);
                 $pb = $this->skala_kepentingan($persepsi[0],$braile[0],$persepsi[1],$braile[1]);
                 $mv = $this->skala_kepentingan($moment[0],$visus[0],$moment[1],$visus[1]);
                 $pv = $this->skala_kepentingan($persepsi[0],$braile[0],$persepsi[1],$braile[1]);
                 $pm = $this->skala_kepentingan($persepsi[0],$moment[0],$persepsi[1],$moment[1]);

                 $pairwise = $this->matriks_pairwise($vb,$mb,$pb,$mv,$pv,$pm);

                 $gabungan = array();
                 $gabungan[] = array(0.61,0.54,0.57,0.16);
                 $gabungan[] = array(0.30,0.30,0.29,0.22);
                 $gabungan[] = array(0.26,0.16,0.14,0.62);

                 $v_eigen = array();
                 $v_eigen[] = array($pairwise[5]);
                 $v_eigen[] = array($pairwise[6]);
                 $v_eigen[] = array($pairwise[7]);
                 $v_eigen[] = array($pairwise[8]);

                 $spk = $this->perkalian_matriks($gabungan,$v_eigen);

                 $urut = max($spk[0][0],$spk[1][0],$spk[2][0]);
                 if ($urut == $spk[0][0]) {
                 	$kelas = 'Total Blindness';
                 }elseif ($urut == $spk[1][0]) {
                 	$kelas = 'Sedang';
                 }elseif ($urut == $spk[2][0]) {
                 	$kelas = 'Low Vision';
                 }
var_dump($urut);
				$datas = array(
                    //"idimport"=> $rowData[0][0],
                    "sisID"=> $rowData[0][0],
                    "sisNama"=> $rowData[0][1],
                    "persepsi"=> $rowData[0][2],
                    "visus"=> $rowData[0][3],
                    "moment"=> $rowData[0][4],
                    "braile"=> $rowData[0][5],
                    "tkt_disb" => $kelas,
                    "tgl_upload"=> date("Y-m-d H:i:s")
                );

            	//var_dump($rowData[0][1]);
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("hasil_asesmen",$datas);
                //delete_files($media['file_path']);
                   
            }    	
    }
//// Contoh penggunaan :
//// Matriks A
//$a = array();
//$a[] = array(1, 2, 3);
//$a[] = array(4, 5, 6);
//$a[] = array(7, 8, 9);
////$a[] = array(10, 11, 12);
//
//// Matriks B
//$b = array();
//$b[] = array(1);
//$b[] = array(5);
//$b[] = array(9);

//kalikan
//$hasil = perkalian_matriks($a, $b);
//echo "<table border='1' cellspacing='0' cellpadding='5'>";
//for ($i=0; $i<sizeof($hasil); $i++) {
	//echo "<tr>";
	//for ($j=0; $j<sizeof($hasil[$i]); $j++) {
		//echo "<td>". round($hasil[$i][$j], 4) ."</td>";
	//}
	//echo "</tr>";
//}
//echo "</table>";
}


