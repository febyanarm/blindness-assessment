<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Orangtua extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->database();
        $this->load->library('session');
        $this->load->helper('download');
        if (($this->session->userdata('namauser')=="")&&($this->session->userdata('role')!="orangtua")) {
            $this->session->unset_userdata('namauser');
            session_destroy();

            echo"Anda Belum Login";
            redirect('Welcome');
        }
        $this->load->model('Model_siswa');
        $this->load->model('Model_spk');
    }
    public function dashboard()
    {
        $data['hal'] = 'dashboard';
        $data1['hitung_jkL'] = $this->Model_siswa->hitung_jkL();
        $data1['hitung_jkP'] = $this->Model_siswa->hitung_jkP();
        $data1['jml_siswa'] = $this->Model_siswa->jml_siswa();
        $data1['data_siswa'] = $this->Model_siswa->data_siswa();
        $data['content'] = $this->load->view('orangtua/dashboard', $data1, TRUE);
        $this->load->view('skin/index', $data);
    }

    public function logout() {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role');
        session_destroy();
        redirect();
    }
}