<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
    <!-- Generated: 2018-04-16 09:29:05 +0200 -->
    <title>ADMIN - SLB YPKR A Cicalengka</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/bs_style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">


    <script src="<?php echo base_url();?>assets/admin/js/require.min.js"></script>
    <script>
      requirejs.config({
          baseUrl: '../assets/admin'
      });
    </script>
    <!-- Dashboard Core -->
    <link href="<?php echo base_url();?>assets/admin/css/dashboard.css" rel="stylesheet" />
    <script src="<?php echo base_url();?>assets/admin/js/dashboard.js"></script>
    <!-- c3.js Charts Plugin -->
    <link href="<?php echo base_url();?>assets/admin/plugins/charts-c3/plugin.css" rel="stylesheet" />
    <script src="<?php echo base_url();?>assets/admin/plugins/charts-c3/plugin.js"></script>
    <!-- Google Maps Plugin -->
    <link href="<?php echo base_url();?>assets/admin/plugins/maps-google/plugin.css" rel="stylesheet" />
    <script src="<?php echo base_url();?>assets/admin/plugins/maps-google/plugin.js"></script>
    <!-- Input Mask Plugin -->
    <script src="<?php echo base_url();?>assets/admin/plugins/input-mask/plugin.js"></script>
    <!-- Datatables Core -->
    <script src="<?php echo base_url();?>assets/admin/plugins/DataTables/plugin.js"></script>

 <!--    <!-- datatables -->

      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/DataTables/Buttons-1.5.2/css/buttons.bootstrap4.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/DataTables/Responsive-2.2.2/css/responsive.bootstrap4.min.css">

    <!-- end of datatables -->

  </head>
  <body class="">
    <div class="page">
      <div class="page-main">
        <div class="header py-4">
          <div class="container">
            <div class="d-flex">
              <a class="header-brand" href="./index.html">
                <!-- <img src="./demo/brand/tabler.svg" class="header-brand-img" alt="tabler logo"> -->
                SISTEM ASESMEN - SLB YPKR A
              </a>
              <div class="d-flex order-lg-2 ml-auto">
                <div class="dropdown">
                  <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                    <span class="avatar" style="background-image: url(./demo/faces/female/25.jpg)"></span>
                    <span class="ml-2 d-none d-lg-block">
                      <span class="text-default"><?php echo $this->session->userdata('namapengguna'); ?></span>
                      <small class="text-muted d-block mt-1">

                      <?php if($this->session->userdata('role') == 'admin') { ?>
                      Administrator
                      <?php }else{?>
                        Orang Tua
                      <?php } ?>
                      </small>
                    </span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                    <a class="dropdown-item" href="<?php echo base_url();?>Logout">
                      <i class="dropdown-icon fe fe-log-out"></i> Keluar dai aplikasi
                    </a>
                  </div>
                </div>
              </div>
              <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
              </a>
            </div>
          </div>
        </div>
        <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                  <?php if($this->session->userdata('role') == 'admin') { ?>
                  <li class="nav-item">
                    <a href="<?php echo base_url(); ?>Admin/dashboard" class="nav-link <?php if($hal == 'dashboard'){ echo 'active';} ?>"><i class="fe fe-home"></i> Home</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo base_url(); ?>Admin/pendaftaran" class="nav-link <?php if($hal == 'pendaftaran'){ echo 'active';} ?>"><i class="fe fe-image"></i> Pendaftaran</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo base_url(); ?>Admin/asesmen" class="nav-link <?php if($hal == 'asesmen'){ echo 'active';} ?>"><i class="fe fe-file-text"></i> Asesmen</a>
                  </li>
                <?php }else if($this->session->userdata('role') == 'orangtua'){ ?>
                  <li class="nav-item">
                    <a href="<?php echo base_url(); ?>Admin/dashboard" class="nav-link <?php if($hal == 'dashboard'){ echo 'active';} ?>"><i class="fe fe-home"></i> Home</a>
                  </li>
                <?php } ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="my-3 my-md-5">
          <div class="container">

            <?php echo $content; ?>
            
          </div>
        </div>
      </div>
      <footer class="footer">
        <div class="container">
          <div class="row align-items-center flex-row-reverse">
            <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
              Copyright © 2018 <a href=".">Tabler</a>. Theme by <a href="https://codecalm.net" target="_blank">codecalm.net</a> All rights reserved.
            </div>
          </div>
        </div>
      </footer>
    </div>

    <!-- datatables -->

    <script>
     require(['datatables', 'jquery'], function(datatables, $) {
      $(document).ready(function () {
        var tabel = $('#data_member').DataTable({
          autoWidth: true,
          responsive: true,
          dom: 'Bfrtip',
          scrollX: true,
          lengthChange: false,
          buttons: [{
              extend: 'pageLength'
            },
            {
              extend: 'collection',
              text: 'Ekspor',
              buttons: [{
                  extend: 'excel',
                  text: 'Excel'
                },
                {
                  extend: 'csv',
                  text: 'CSV'
                },
                {
                  extend: 'pdf',
                  text: 'PDF'
                },
                {
                  extend: 'print',
                  text: 'cetak'
                }
              ]
            }
          ]
        });
      });
    });
     
    </script>

    <!-- end of datatables -->

  </body>
</html>