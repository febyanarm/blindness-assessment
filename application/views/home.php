<!doctype html>
<html lang="en" dir="ltr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta http-equiv="Content-Language" content="en" />
  <meta name="msapplication-TileColor" content="#2d89ef">
  <meta name="theme-color" content="#4188c9">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <link rel="icon" href="../favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico" />
  <title>Home - Dashboard Admin</title>
  <link rel="stylesheet" href="../bower_components/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/dashboard.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/DataTables/Buttons-1.5.2/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/DataTables/Responsive-2.2.2/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/dash-style.css">
</head>

<body class="">
  <div class="page">
    <div class="page-main">
      <div class="header py-4">
        <div class="container">
          <div class="d-flex">
            <a class="header-brand" href="./index.html">
              <img src="<?php echo base_url();?>/assets/images/logo1.png" class="header-brand-img">
            </a>
            <div class="d-flex order-lg-2 ml-auto">
              <div class="dropdown">
                <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                  <span class="avatar" style="background-image: url(../demo/faces/female/25.jpg)"></span>
                  <span class="ml-2 d-none d-lg-block">
                    <span class="text-default">Febyana Rizki Maharani</span>
                    <small class="text-muted d-block mt-1">Administrator</small>
                  </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                  <a class="dropdown-item" href="./profile.html">
                    <i class="dropdown-icon fe fe-user"></i> Profile
                  </a>
                  <a class="dropdown-item" href="./password.html">
                    <i class="dropdown-icon fe fe-lock"></i> Change Password
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="../index.html">
                    <i class="dropdown-icon fe fe-log-out"></i> Sign out
                  </a>
                </div>
              </div>
            </div>
            <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
              <span class="header-toggler-icon"></span>
            </a>
          </div>
        </div>
      </div>
      <div class="header collapse d-lg-flex p-0 sticky-top" id="headerMenuCollapse">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg order-lg-first">
              <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                <li class="nav-item">
                  <a href="./index.html" class="nav-link active">
                    <i class="fe fe-home"></i> Home</a>
                </li>
                <li class="nav-item">
                  <a href="./mentor.html" class="nav-link">
                    <i class="fe fe-user"></i> Pendaftaran</a>
                </li>
                <li class="nav-item">
                  <a href="./mentoring.html" class="nav-link">
                    <i class="fe fe-book"></i> Asesmen</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="">
        <div class="container">
          <div class="row">
            <!-- isi -->

            <!-- end of isi -->
          </div>
        </div>
      </div>
    </div>
    <footer class="footer">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-12 mt-3 mt-lg-0">
            Copyright © 2018
            <a href="#">Billionaire Store</a>. Theme by
            <a href="https://codecalm.net" target="_blank">codecalm.net</a> All rights reserved.
          </div>
        </div>
      </div>
    </footer>
  </div>

  <script src="<?php echo base_url();?>/assets/js/vendors/jquery-3.2.1.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/vendors/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/Responsive-2.2.2/js/responsive.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/Buttons-1.5.2/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/Buttons-1.5.2/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/JSZip-2.5.0/jszip.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/pdfmake-0.1.36/pdfmake.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/pdfmake-0.1.36/vfs_fonts.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/Buttons-1.5.2/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/Buttons-1.5.2/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/Buttons-1.5.2/js/buttons.colVis.min.js"></script>
  <script src="<?php echo base_url();?>assets/inputmask/js/dependencyLibs/inputmask.dependencyLib.js"></script>
  <script src="<?php echo base_url();?>assets/inputmask/js/inputmask.js"></script>
  <script src="<?php echo base_url();?>assets/inputmask/js/inputmask.extensions.js"></script>
  <script src="<?php echo base_url();?>assets/inputmask/js/inputmask.numeric.extensions.js"></script>
  <script src="<?php echo base_url();?>assets/inputmask/js/bindings/inputmask.binding.js"></script>
  <script src="<?php echo base_url();?>assets/inputmask/js/dependencyLibs/inputmask.dependencyLib.js"></script>
<!-- <script src="<?php echo base_url();?>dist/inputmask/inputmask.js"></script>
<script src="<?php echo base_url();?>dist/inputmask/inputmask.extensions.js"></script>
<script src="<?php echo base_url();?>dist/inputmask/inputmask.numeric.extensions.js"></script>
<script src="<?php echo base_url();?>dist/inputmask/bindings/inputmask.binding.js"></script> -->
  <script>
    $(document).ready(function(){
      $(":input").inputmask();
      or
      Inputmask().mask(document.querySelectorAll("input"));
    });
        
    $(document).ready(function () {
      $('.form-upload input').change(function () {
        $('.caption-upload p').text(this.files.length + " file(s) selected");
      });
      var tabel = $('#data_member').DataTable({
        autoWidth: true,
        responsive: true,
        dom: 'Bfrtip',
        scrollX: true,
        lengthChange: false,
        buttons: [{
            extend: 'pageLength'
          },
          {
            extend: 'collection',
            text: 'Export',
            buttons: [{
                extend: 'excel',
                text: 'Excel'
              },
              {
                extend: 'csv',
                text: 'CSV'
              },
              {
                extend: 'pdf',
                text: 'PDF'
              },
              {
                extend: 'print',
                text: 'Print'
              }
            ]
          }
        ]
      });
    });
  </script>
</body>

</html>