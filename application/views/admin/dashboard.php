<div class="page-header">
              <h1 class="page-title">
                Dashboard
              </h1>
            </div>
            <div class="row">

                  <div class="col-sm-3">
                    <div class="card">
                      <div class="card-header">
                        <h3 class="card-title">Grafik perbandingan gender pendaftar</h3>
                      </div>
                      <div class="card-body">
                        <div id="chart-donut" style="height: 12rem;"></div>
                      </div>
                    </div>
                    <script>
                      require(['c3', 'jquery'], function(c3, $) {
                        $(document).ready(function(){
                          var jkL = <?php echo $hitung_jkL; ?>;
                          var jkP = <?php echo $hitung_jkP; ?>;
                           c3.generate({
                            bindto: '#chart-donut', // id of chart wrapper
                            data: {
                              columns: [
                                  // each columns data
                                ['data1', jkL],
                                ['data2', jkP]
                              ],
                              type: 'donut', // default type of chart
                              colors: {
                                'data1': tabler.colors["green"],
                                'data2': tabler.colors["green-light"]
                              },
                              names: {
                                  // name of each serie
                                'data1': 'Laki-Laki',
                                'data2': 'Perempuan'
                              }
                            },
                            axis: {
                            },
                            legend: {
                                      show: false, //hide legend
                            },
                            padding: {
                              bottom: 0,
                              top: 0
                            },
                          });
                        });
                      });
                    </script>
                  </div>
                  <div class="col-sm-3">
                    <div class="card">
                      <div class="card-body p-3 text-center">
                        <div class="text-right text-green">
                          
                          <i class="fe fe-chevron-up" style="color: white"></i>
                        </div>
                        <div class="h1 m-0">
                          <?php echo $jml_siswa; ?>
                        </div>
                        <div class="text-muted mb-4">Pendaftar</div>
                      </div>
                    </div>
                    <div class="card p-3">
                      <div class="d-flex align-items-center">
                        <span class="stamp stamp-md bg-pink mr-3">
                          <i class="fe fe-user"></i>
                        </span>
                        <div>
                          <h4 class="m-0"><?php echo $hitung_jkL; ?> <small>Orang Laki-Laki</small></h4>
                        </div>
                      </div>
                    </div>
                    <div class="card p-3">
                      <div class="d-flex align-items-center">
                        <span class="stamp stamp-md bg-blue mr-3">
                          <i class="fe fe-user"></i>
                        </span>
                        <div>
                          <h4 class="m-0"><?php echo $hitung_jkP; ?> <small>Orang Perempuan</small></h4>
                        </div>
                      </div>
                    </div>
                  </div>

              <div class="col-lg-6">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Data Terbaru</h3>
                  </div>
                  <div class="table-responsive">
                    <table class="table card-table table-striped table-vcenter">
                      <thead>
                        <tr>
                        <th>NIS</th>
                        <th>Nama Siswa</th>
                        <th>Jenis Kelamin</th>
                        <th>Tanggal Lahir</th>
                        <th>No Telp/HP</th>
                        <th>Email</th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($data_siswa as $r): ?>                       
                        <tr>
                          <td><?php echo $r->sisID; ?></td>
                          <td><?php echo $r->sisNama; ?></td>
                          <td><?php echo $r->sisJK; ?></td>
                          <td><?php echo $r->sisLahir; ?></td>
                          <td><?php echo $r->sisTlp; ?></td>
                          <td><?php echo $r->sisEmail; ?></td>
                        </tr>
                      <?php endforeach ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>