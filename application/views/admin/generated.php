            <div class="row hasil-spk col-md-12" style="position: absolute;">

            <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Data Terbaru</h3>
                  </div>
                <div class="card-body">
                  <table id="data_member" class="table table-striped table-bordered dt-responsive nowrap w-100">
                      <thead>
                        <tr>
                        <th>Nomor Induk Siswa</th>
                        <th>Nama Siswa</th>
                        <th>Persepsi</th>
                        <th>Visus</th>
                        <th>Moment</th>
                        <th>Braile</th>
                        <th>Hasil Keputusan</th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($ambildata as $r): ?>                       
                        <tr>
                                        <td><?php echo $r->sisID; ?></td>
                                        <td><?php echo $r->sisNama; ?></td>
                                        <td><?php echo $r->persepsi; ?></td>
                                        <td><?php echo $r->visus; ?></td>
                                        <td><?php echo $r->moment; ?></td>
                                        <td><?php echo $r->braile; ?></td>
                                        <td><?php echo $r->tkt_disb; ?></td>
                        </tr>
                      <?php endforeach ?>
                      </tbody>
                    </table>
                  </div>
                </div>
        	</div>
