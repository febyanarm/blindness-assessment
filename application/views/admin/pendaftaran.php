
<div class="col-lg-12">
              <div class="page-header">
                <h1 class="page-title">Data Pendaftaran</h1>
              </div>
              <div class="card">
                <div class="card-header">
                  <div class="card-options">
                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#addmember">
                      <i class="fe fe-user-plus mr-2"></i>Daftarkan secara Masal</a>
                  </div>
                </div>
                <div class="card-body">
                  <table id="data_member" class="table table-striped table-bordered dt-responsive nowrap w-100">
                    <thead>
                      <tr>
                        <th>NIS</th>
                        <th>Nama Siswa</th>
                        <th>Jenis Kelamin</th>
                        <th>Nama Ayah</th>
                        <th>Nama Ibu</th>
                        <th>Tanggal Lahir</th>
                        <th>No Telp/HP</th>
                        <th>Agama</th>
                        <th>Email</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                    	<?php foreach ($data_siswa as $r): ?>                    		
	                      <tr>
	                        <td><?php echo $r->sisID; ?></td>
	                        <td><?php echo $r->sisNama; ?></td>
	                        <td><?php echo $r->sisJK; ?></td>
	                        <td><?php echo $r->sisAyah; ?></td>
	                        <td><?php echo $r->sisIbu; ?></td>
	                        <td><?php echo $r->sisLahir; ?></td>
	                        <td><?php echo $r->sisTlp; ?></td>
	                        <td><?php echo $r->sisAgama; ?></td>
	                        <td><?php echo $r->sisEmail; ?></td>
	                        <td>
	                          <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#editmember">
	                            <i class="fe fe-edit mr-1"></i>Edit Data
	                          </a>
	                        </td>
	                      </tr>
                    	<?php endforeach ?>
                    </tbody>
                  </table>
                </div>
              </div>

             <!--  UPLOAD MASAL -->

                  <div class="modal fade" id="addmember" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                        	<h5 class="modal-title">
                        		Formulir Pendaftaran
                        	</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          </button>
                        </div>
                        <div class="modal-body">
                          <div class="row">

	      			<div class="col-md-6" style="border-right: 1px dashed #aaa;">
	      				<h2>Tambah Manual</h2>
	      				<form  enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url();?>index.php/Admin/dftr_manual" method="POST">
								<div class="form-group">
									<input type="text" class="form-control" name="sisNama" placeholder="Nama Siswa">
								</div>
								<div class="custom-controls-stacked">
		                          <label class="custom-control custom-radio custom-control-inline">
		                            <input type="radio" class="custom-control-input" name="jk" value="L" checked>
		                            <span class="custom-control-label">Laki-laki</span>
		                          </label>
		                          <label class="custom-control custom-radio custom-control-inline">
		                            <input type="radio" class="custom-control-input" name="jk" value="P">
		                            <span class="custom-control-label">Perempuan</span>
		                          </label>
		                        </div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<input type="date" class="form-control" name="tgl_lahir" placeholder="Tanggal Lahir">
										</div>
										<div class="col-md-6">
											<input type="text" class="form-control" name="agama" placeholder="Agama">
										</div>
									</div>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="hp" placeholder="Telp/HP">
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
									<input type="text" class="form-control" name="ayah" placeholder="Nama Ayah">
										</div>
										<div class="col-md-6">
									<input type="text" class="form-control" name="ibu" placeholder="Nama Ibu">
										</div>
									</div>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="email" placeholder="Email">
								</div>
								<div class="form-group">
									<input type="submit" class="btn btn-submit" name="status" value="DAFTAR">
								</div>
						</form>
					</div>
                            <div class="col-lg-6 col-md-6">
                            	<div class="row">
                            		<div class="col-md-6">
			                          <h3>
			                            Unggah Data Masal :
			                          </h3>
			                      	</div>

                            		<div class="col-md-6">
		                              <a href="<?php echo base_url().'Admin/download/pendaftaran.xlsx';?>" class="btn btn-info btn-sm pull-right" style="margin: 0px 30px">
		                                <i class="fe fe-download mr-1"></i>Unduh Template Excel
		                              </a>
			                      	</div>

		                          </div>
                              <form class="upload" action="<?php echo base_url();?>index.php/Admin/daftarkan" method="POST"  enctype="multipart/form-data">
                                <div class="form-upload mb-3">
                                  <input type="file" name="file">
                                  <div class="caption-upload text-center">
                                    <i class="fas fa-cloud-upload-alt fa-3x text-primary mb-2"></i>
                                    <p class="font-weight-bold">Tarik file .xls anda <span class="text-primary">ke sini</span></p>
                                  </div>                              
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Unggah</button>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

            <!-- END OF UPLOAD MASAL -->

            <!-- ONE BY ONE -->

              <div class="modal fade" id="editmember" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                      <h5 class="modal-title">
                        Hitung Tingkat Disabilitas
                      </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-lg-7 col-md-12 mb-5">
                          <form action="#" method="POST">
                            <h3>Unggah Manual</h3>
                            <div class="form-group">
                              <label>Nama Siswa</label>
                              <input type="text" class="form-control" value="UD1234123" readonly>
                            </div>
                            <div class="form-group">
                              <label>Persepsi Cahaya</label>
                              <input type="text" class="form-control" placeholder="Jawaban Benar/Total Pertanyaan (q/Q)"  data-inputmask="'mask': '99/99'" required>
                            </div>
                            <div class="form-group">
                              <label>Tes Snellen (Visus)</label>
                              <input type="text" class="form-control" placeholder="Jarak Pandang/Jarak Benda Sesungguhnya (r/R)"  data-inputmask="'mask': '99/99'" required>
                            </div>
                            <div class="form-group">
                              <label>Saat Terjadinya Kecelakaan (Usia)</label>
                              <input type="text" class="form-control" placeholder="10"  data-inputmask="'mask': '99'" required>
                            </div>
                            <div class="form-group">
                              <label>Skor Tes Braille</label>
                              <input type="text" class="form-control" placeholder="10" data-inputmask="'mask': '99'" required>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Tambahkan</button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- END OF ONE BY ONE -->


              <div class="modal fade" id="delmember" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">
                        Delete Member
                      </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      </button>
                    </div>
                    <form action="#" method="POST">
                      <div class="modal-body">
                        Anda yakin ingin menghapus Member
                        <strong>Adi</strong>?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-primary">Hapus</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

