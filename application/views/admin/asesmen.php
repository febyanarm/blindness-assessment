<div class="col-lg-12">
              <div class="page-header">
                <h1 class="page-title">Member</h1>
              </div>
              <div class="card">
                <div class="card-header">
                  <div class="card-options">
                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#addmember">
                      <i class="fe fe-user-plus mr-2"></i>Lakukan Asesmen Masal</a>
                  </div>
                </div>
                <div class="card-body">
                  <table id="data_member" class="table table-striped table-bordered dt-responsive nowrap w-100">
                    <thead>
                      <tr>
                        <th>Nomor Induk Siswa</th>
                        <th>Nama Siswa</th>
                        <th>Persepsi</th>
                        <th>Visus</th>
                        <th>Moment</th>
                        <th>Braile</th>
                        <th>Hasil Keputusan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($ambildata as $r): ?>
                      <tr>
                        <td><?php echo $r->sisID; ?></td>
                        <td><?php echo $r->sisNama; ?></td>
                        <td><?php echo $r->persepsi; ?></td>
                        <td><?php echo $r->visus; ?></td>
                        <td><?php echo $r->moment; ?></td>
                        <td><?php echo $r->braile; ?></td>
                        <td><?php echo $r->tkt_disb; ?></td>
                        <td>
                          <a href="#" id="pilih" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editmember" 
                          sisID="<?php echo $r->sisID; ?>"
                          sisNama="<?php echo $r->sisNama; ?>"
                          >
                            <i class="fe fe-edit mr-1"></i>Lakukan Asesmen
                          </a>
                        </td>
                      </tr>
                      <?php endforeach ?>
                    </tbody>
                  </table>
                </div>
              </div>



             <!--  UPLOAD MASAL -->

                  <div class="modal fade" id="addmember" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                         <h5 class="modal-title">
                            Unggah Masal
                          </h5>
                              <a href="<?php echo base_url().'Admin/download/asesmen.xlsx';?>" class="btn btn-info btn-sm pull-right" style="margin: 0px 30px">
                                    <i class="fe fe-download mr-1"></i>Unduh Template Excel
                                  </a>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          </button>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                            <div class="col-lg-9 col-md-9">
                              <form class="upload" action="<?php echo base_url();?>index.php/Dashboard/generate" method="POST"  enctype="multipart/form-data">
                                <div class="form-upload mb-3">
                                  <input type="file" name="file">
                                  <div class="caption-upload text-center">
                                    <i class="fas fa-cloud-upload-alt fa-3x text-primary mb-2"></i>
                                    <p class="font-weight-bold">Tarik file .xls anda <span class="text-primary">ke sini</span></p>
                                  </div>                              
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Unggah</button>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

            <!-- END OF UPLOAD MASAL -->

            <!-- ONE BY ONE -->

              <div class="modal fade" id="editmember" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                      <h5 class="modal-title">
                        Hitung Tingkat Disabilitas
                      </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-lg-7 col-md-12 mb-5">
                          <form action="#" method="POST">
                            <h3>Unggah Manual</h3>
                            <div class="form-group">
                              <label>NIS</label>
                              <input type="text" class="form-control" readonly id="sisID">
                            </div>
                            <div class="form-group">
                              <label>Nama Siswa</label>
                              <input type="text" class="form-control" readonly id="sisNama">
                            </div>
                            <div class="form-group">
                              <label>Persepsi Cahaya</label>
                              <input type="text" class="form-control" placeholder="Jawaban Benar/Total Pertanyaan (q/Q)"  data-inputmask="'mask': '99/99'" required>
                            </div>
                            <div class="form-group">
                              <label>Tes Snellen (Visus)</label>
                              <input type="text" class="form-control" placeholder="Jarak Pandang/Jarak Benda Sesungguhnya (r/R)"  data-inputmask="'mask': '99/99'" required>
                            </div>
                            <div class="form-group">
                              <label>Saat Terjadinya Kecelakaan (Usia)</label>
                              <input type="text" class="form-control" placeholder="10"  data-inputmask="'mask': '99'" required>
                            </div>
                            <div class="form-group">
                              <label>Skor Tes Braille</label>
                              <input type="text" class="form-control" placeholder="10" data-inputmask="'mask': '99'" required>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Tambahkan</button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- END OF ONE BY ONE -->


              <div class="modal fade" id="delmember" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">
                        Delete Member
                      </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      </button>
                    </div>
                    <form action="#" method="POST">
                      <div class="modal-body">
                        Anda yakin ingin menghapus Member
                        <strong>Adi</strong>?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-primary">Hapus</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

<script type="text/javascript">
  
  $(document).on('click', '#pilih', function (e) {
    alert('berhasil');
    document.getElementById('sisID').value = $(this).attr('sisID');
    document.getElementById('sisNama').value = $(this).attr('sisNama');
  });
  
</script>