<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Result</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
        <!--        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-theme.min.css">-->


        <!--For Plugins external css-->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/plugins.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/roboto-railway-webfont.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/linearicons-web-font.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/magnific-popup.css">

        <!--Theme custom css -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css" />

        <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body class="dashboard">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

		<div class='preloader'><div class='loaded'>&nbsp;</div></div>
		<header id="home" class="home">
                <div class="container text-center">
                    <div class="row">
						<div class="div-menu">
                            <header class="cd-header">
                                <div id="cd-logo">
                                    <a href="<?php echo base_url();?>index.php/Dashboard"><img src="<?php echo base_url();?>assets/images/logo.png" alt="Logo"></a>
                                </div>
                                <a class="cd-menu-logout" href="<?php echo base_url();?>index.php/Dashboard/logout">Logout</a>
                            </header>
                        </div>
                    </div>
                </div>
        </header>
        <div class="row hasil-spk col-md-6 col-md-offset-3" style="position: absolute;">
        	<table class="table table-striped">
            <thead>
              <tr>
                <th>Nomor Induk Siswa</th>
                <th>Nama Siswa</th>
                <th>Persepsi</th>
                <th>Visus</th>
                <th>Moment</th>
                <th>Braile</th>
                <th>Hasil Keputusan</th>
              </tr>
            </thead>
            <tbody>
                                <?php foreach ($ambildata as $row): ?>
                                    <tr>
                                        <td><?php echo $row->sisID; ?></td>
                                        <td><?php echo $row->sisNama; ?></td>
                                        <td><?php echo $row->persepsi; ?></td>
                                        <td><?php echo $row->visus; ?></td>
                                        <td><?php echo $row->moment; ?></td>
                                        <td><?php echo $row->braile; ?></td>
                                        <td><?php echo $row->tkt_disb; ?></td>
                                    </tr>
                                <?php endforeach ?>
            </tbody>
        </table>
		</div>


        <script src="<?php echo base_url();?>assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/vendor/bootstrap.min.js"></script>

        <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.magnific-popup.js"></script>
        <script src="<?php echo base_url();?>assets/js/modernizr.js"></script>

        <script src="<?php echo base_url();?>assets/js/main.js"></script>
        <script type="text/javascript">
        	$(document).ready(function(){
			  $('form input').change(function () {
			    $('form p').text(this.files.length + " file(s) selected");
			  });
			});	
        </script>
    </body>
</html>
