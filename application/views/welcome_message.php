<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
        <!--        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-theme.min.css">-->


        <!--For Plugins external css-->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/plugins.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/roboto-railway-webfont.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/linearicons-web-font.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/magnific-popup.css">

        <!--Theme custom css -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css" />

        <script src="<?php echo base_url();?>assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

		<div class='preloader'><div class='loaded'>&nbsp;</div></div>


        <nav id="main-nav">
        	<div class="row">
        		<div class="col-md-6 col-md-offset-3 login">
		        	<form class="form-group" action="<?php echo base_url(); ?>index.php/auth/cek_login" method="POST">
		        		<div class="row">
		        			<div class="col-md-3">
				        		<label>Nama User</label>
				        	</div>
		        			<div class="col-md-9">
				        		<input type="text" name="namauser" class="form-control">
				        	</div>
		        		</div>
		        		<div class="row">
		        			<div class="col-md-3">
		        				<label>Kata Sandi</label>
				        	</div>
		        			<div class="col-md-9">
		        				<input type="password" name="katasandi" class="form-control">
				        	</div>
		        		</div>
		        		<input type="submit" name="" value="MASUK" class="btn btn-info pull-right login-btn">
		        	</form>
		        </div>
		    </div>
            <!--<ul>
                <li><a href="#home"><span>Home</span></a></li>
                <li><a href="#about"><span>About Me</span></a></li>
                <li><a href="#features"><span>Features</span></a></li>
                <li><a href="#experience"><span>Experience</span></a></li>
                <li><a href="#portfolio"><span>Portfolio</span></a></li>
                
            </ul>-->
            <a href="#0" class="cd-close-menu">Close<span></span></a>
        </nav>

        <!--Home page style-->
        <header id="home" class="home home-main-content">
            <div class="overlay sections">
                <div class="container text-center">
                    <div class="row"><!--
                        <div class="div-menu">
                            <header class="cd-header">
                                <div id="cd-logo">
                                    <a href="index.html"><img src="assets/images/logo.png" alt="Logo"></a>
                                </div>
                                <a class="cd-menu-trigger" href="#main-nav"><span></span></a>
                            </header>
                        </div>
-->

                        <div class="home-wrapper">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                                <div class="home-content">
                                    <h1>RANER</h1>
                                    <p>we help you <b style="color: white;">see</b> their precious talents</p>
                                <a class="cd-menu-trigger" href="#main-nav">LOGIN</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!--Footer-->
        <footer id="footer" class="footer">
            <div class="container text-center">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                        <div class="main-footer">

                            <div class="social">
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-behance"></i></a>
                                <a href="#"><i class="fa dribbble fa-dribbble"></i></a>
                            </div>

                            <p>Made with <i class="fa fa-heart"></i> by <a target="_blank" href="http://bootstrapthemes.co"> Bootstrap Themes </a>2016. All rights reserved.</p>

                        </div>
                    </div>
                </div>
            </div>

        </footer>


     
        <script src="<?php echo base_url();?>assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/vendor/bootstrap.min.js"></script>

        <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.magnific-popup.js"></script>
        <script src="<?php echo base_url();?>assets/js/modernizr.js"></script>

        <script src="<?php echo base_url();?>assets/js/main.js"></script>
    </body>
</html>
