<!doctype html>
<html lang="en" dir="ltr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta http-equiv="Content-Language" content="en" />
  <meta name="msapplication-TileColor" content="#2d89ef">
  <meta name="theme-color" content="#4188c9">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <link rel="icon" href="../favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico" />
  <title>Dashboard Admin</title>
  <link rel="stylesheet" href="../bower_components/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/dashboard.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/DataTables/Buttons-1.5.2/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/DataTables/Responsive-2.2.2/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/dash-style.css">
</head>

<body class="">
  <div class="page">
    <div class="page-main">
      <div class="header py-4">
        <div class="container">
          <div class="d-flex">
            <a class="header-brand" href="./index.html">
              <img src="<?php echo base_url();?>/assets/images/logo1.png" class="header-brand-img">
            </a>
            <div class="d-flex order-lg-2 ml-auto">
              <div class="dropdown d-block d-flex">
                <a class="nav-link icon" data-toggle="dropdown">
                  <i class="fe fe-bell"></i>
                  <span class="nav-unread"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                  <a href="#" class="dropdown-item d-flex">
                    <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/male/41.jpg)"></span>
                    <div>
                      Tugas Materi 1 Belum Selesai
                      <div class="small text-muted">by Admin</div>
                    </div>
                  </a>
                  <a href="#" class="dropdown-item d-flex">
                    <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/female/1.jpg)"></span>
                    <div>
                      Jadwal Baru Sudah Di Upload
                      <div class="small text-muted">by Admin</div>
                    </div>
                  </a>
                  <a href="#" class="dropdown-item d-flex">
                    <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/female/18.jpg)"></span>
                    <div>
                      Tugas Materi 1 Belum Selesai
                      <div class="small text-muted">by Admin</div>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a href="#" class="dropdown-item text-center text-muted-dark">Lihat Semua</a>
                </div>
              </div>
              <div class="dropdown">
                <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                  <span class="avatar" style="background-image: url(../demo/faces/female/25.jpg)"></span>
                  <span class="ml-2 d-none d-lg-block">
                    <span class="text-default">Adi Rizky</span>
                    <small class="text-muted d-block mt-1">Administrator</small>
                  </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                  <a class="dropdown-item" href="./profile.html">
                    <i class="dropdown-icon fe fe-user"></i> Profile
                  </a>
                  <a class="dropdown-item" href="./password.html">
                    <i class="dropdown-icon fe fe-lock"></i> Change Password
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="../index.html">
                    <i class="dropdown-icon fe fe-log-out"></i> Sign out
                  </a>
                </div>
              </div>
            </div>
            <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
              <span class="header-toggler-icon"></span>
            </a>
          </div>
        </div>
      </div>
      <div class="header collapse d-lg-flex p-0 sticky-top" id="headerMenuCollapse">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg order-lg-first">
              <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                <li class="nav-item">
                  <a href="./index.html" class="nav-link">
                    <i class="fe fe-home"></i> Home</a>
                </li>
                <li class="nav-item">
                  <a href="./mentor.html" class="nav-link">
                    <i class="fe fe-user"></i> Mentor</a>
                </li>
                <li class="nav-item">
                  <a href="./mentoring.html" class="nav-link">
                    <i class="fe fe-book"></i> Mentoring</a>
                </li>
                <li class="nav-item">
                  <a href="./member.html" class="nav-link active">
                    <i class="fe fe-users"></i> Member</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="page-header">
                <h1 class="page-title">Member</h1>
              </div>
              <div class="card">
                <div class="card-header">
                  <div class="card-options">
                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#addmember">
                      <i class="fe fe-user-plus mr-2"></i>Lakukan Asesmen Masal</a>
                  </div>
                </div>
                <div class="card-body">
                  <table id="data_member" class="table table-striped table-bordered dt-responsive nowrap w-100">
                    <thead>
                      <tr>
                        <th>ID Member</th>
                        <th>Nama Member</th>
                        <th>Position</th>
                        <th>Office</th>
                        <th>Age</th>
                        <th>Option</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Adi</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>
                          <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#editmember">
                            <i class="fe fe-edit mr-1"></i>Lakukan Asesmen
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

             <!--  UPLOAD MASAL -->

                  <div class="modal fade" id="addmember" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title">
                            Unggah Masal
                          </h5>
                              <a href="#" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#editmember" style="margin: 0px 30px">
                                <i class="fe fe-download mr-1"></i>Unduh Template Excel
                              </a>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          </button>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                            <div class="col-lg-12 col-md-12">
                              <form class="upload" action="<?php echo base_url();?>index.php/Dashboard/generate" method="POST"  enctype="multipart/form-data">
                                <div class="form-upload mb-3">
                                  <input type="file" name="file">
                                  <div class="caption-upload text-center">
                                    <i class="fas fa-cloud-upload-alt fa-3x text-primary mb-2"></i>
                                    <p class="font-weight-bold">Tarik file .xls anda <span class="text-primary">ke sini</span></p>
                                  </div>                              
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Unggah</button>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

            <!-- END OF UPLOAD MASAL -->

            <!-- ONE BY ONE -->

              <div class="modal fade" id="editmember" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                      <h5 class="modal-title">
                        Hitung Tingkat Disabilitas
                      </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-lg-7 col-md-12 mb-5">
                          <form action="#" method="POST">
                            <h3>Unggah Manual</h3>
                            <div class="form-group">
                              <label>Nama Siswa</label>
                              <input type="text" class="form-control" value="UD1234123" readonly>
                            </div>
                            <div class="form-group">
                              <label>Persepsi Cahaya</label>
                              <input type="text" class="form-control" placeholder="Jawaban Benar/Total Pertanyaan (q/Q)"  data-inputmask="'mask': '99/99'" required>
                            </div>
                            <div class="form-group">
                              <label>Tes Snellen (Visus)</label>
                              <input type="text" class="form-control" placeholder="Jarak Pandang/Jarak Benda Sesungguhnya (r/R)"  data-inputmask="'mask': '99/99'" required>
                            </div>
                            <div class="form-group">
                              <label>Saat Terjadinya Kecelakaan (Usia)</label>
                              <input type="text" class="form-control" placeholder="10"  data-inputmask="'mask': '99'" required>
                            </div>
                            <div class="form-group">
                              <label>Skor Tes Braille</label>
                              <input type="text" class="form-control" placeholder="10" data-inputmask="'mask': '99'" required>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Tambahkan</button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!-- END OF ONE BY ONE -->


              <div class="modal fade" id="delmember" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">
                        Delete Member
                      </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      </button>
                    </div>
                    <form action="#" method="POST">
                      <div class="modal-body">
                        Anda yakin ingin menghapus Member
                        <strong>Adi</strong>?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-primary">Hapus</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer class="footer">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-12 mt-3 mt-lg-0">
            Copyright © 2018
            <a href="#">Billionaire Store</a>. Theme by
            <a href="https://codecalm.net" target="_blank">codecalm.net</a> All rights reserved.
          </div>
        </div>
      </div>
    </footer>
  </div>

  <script src="<?php echo base_url();?>/assets/js/vendors/jquery-3.2.1.min.js"></script>
  <script src="<?php echo base_url();?>/assets/js/vendors/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/Responsive-2.2.2/js/responsive.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/Buttons-1.5.2/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/Buttons-1.5.2/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/JSZip-2.5.0/jszip.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/pdfmake-0.1.36/pdfmake.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/pdfmake-0.1.36/vfs_fonts.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/Buttons-1.5.2/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/Buttons-1.5.2/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url();?>/assets/plugins/DataTables/Buttons-1.5.2/js/buttons.colVis.min.js"></script>
  <script src="<?php echo base_url();?>assets/inputmask/js/dependencyLibs/inputmask.dependencyLib.js"></script>
  <script src="<?php echo base_url();?>assets/inputmask/js/inputmask.js"></script>
  <script src="<?php echo base_url();?>assets/inputmask/js/inputmask.extensions.js"></script>
  <script src="<?php echo base_url();?>assets/inputmask/js/inputmask.numeric.extensions.js"></script>
  <script src="<?php echo base_url();?>assets/inputmask/js/bindings/inputmask.binding.js"></script>
  <script src="<?php echo base_url();?>assets/inputmask/js/dependencyLibs/inputmask.dependencyLib.js"></script>
<!-- <script src="<?php echo base_url();?>dist/inputmask/inputmask.js"></script>
<script src="<?php echo base_url();?>dist/inputmask/inputmask.extensions.js"></script>
<script src="<?php echo base_url();?>dist/inputmask/inputmask.numeric.extensions.js"></script>
<script src="<?php echo base_url();?>dist/inputmask/bindings/inputmask.binding.js"></script> -->
  <script>
    $(document).ready(function(){
      $(":input").inputmask();
      or
      Inputmask().mask(document.querySelectorAll("input"));
    });
        
    $(document).ready(function () {
      $('.form-upload input').change(function () {
        $('.caption-upload p').text(this.files.length + " file(s) selected");
      });
      var tabel = $('#data_member').DataTable({
        autoWidth: true,
        responsive: true,
        dom: 'Bfrtip',
        scrollX: true,
        lengthChange: false,
        buttons: [{
            extend: 'pageLength'
          },
          {
            extend: 'collection',
            text: 'Export',
            buttons: [{
                extend: 'excel',
                text: 'Excel'
              },
              {
                extend: 'csv',
                text: 'CSV'
              },
              {
                extend: 'pdf',
                text: 'PDF'
              },
              {
                extend: 'print',
                text: 'Print'
              }
            ]
          }
        ]
      });
    });
  </script>
</body>

</html>