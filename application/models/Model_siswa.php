<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_siswa extends CI_Model {

		public function get_id(){
			// membuat ID atau NIS
                $tahun = date("Ymd");
                $today = date("Y-m-d");
                //$kode = 'MAT';
                $query = $this->db->query("SELECT MAX(sisID) as max_id FROM data_siswa where sisTgl = '$today'"); 
                $row = $query->row_array();
                $max_id = $row['max_id']; 
                $max_id1 =(int) substr($max_id,8,3);
                $peng_ID = $max_id1 +1;
                $maxres_ID = $tahun.sprintf("%03s",$peng_ID);
                return $maxres_ID;
		}
		public function data_siswa() {
			$query = $this->db->get('data_siswa');
			return $query->result();
		}
		public function jml_siswa() {
			$query = $this->db->get('data_siswa');
			return $query->num_rows();
		}
		public function hitung_jkL() {
			$query = $this->db->get_where('data_siswa','sisJK = "L" ');
			return $query->num_rows();
		}
		public function hitung_jkP() {
			$query = $this->db->get_where('data_siswa','sisJK = "P" ');
			return $query->num_rows();
		}
		function dftr_manual(){
			$id_siswa = $this->get_id();
			//collecting data                                
                 $data = array(
                    "sisID"=> $id_siswa,
                    "sisNama"=> $this->input->post('sisNama'),
                    "sisJK"=> $this->input->post('jk'),
                    "sisAyah"=> $this->input->post('ayah'),
                    "sisIbu"=> $this->input->post('ibu'),
                    "sisLahir"=> $this->input->post('tgl_lahir'),
                    "sisTlp"=> $this->input->post('hp'),
                    "sisAgama"=> $this->input->post('agama'),
                    "sisEmail"=> $this->input->post('email')
                );
 				$namauser = trim($this->input->post('sisNama'), " ");  
                $un = substr($namauser,0,5);               
                $data_login = array(
                    "loginID"=> $id_siswa,
                    "namauser"=> $un,
                    "katasandi"=> $this->input->post('hp'),
                    "namapengguna"=> $this->input->post('sisNama'),
                    "role"=> 'orangtua'
                );

                 $input = $this->db->insert("login",$data_login);
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("data_siswa",$data);
		}

	}

?>
