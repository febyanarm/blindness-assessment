require.config({
  shim: {
        'datatables' : {
            deps: ['jquery','bootstrap', 'datatables.net','datatables.net-colVis','datatables.net-print','datatables.net-bs4','datatables.net-buttons-bs','datatables.net-html5','datatables.net-buttons','datatables.net-pdfmake','datatables.net-vfs']  
        },
        'script': {
            deps: ['datatables','datatables.net-responsive']
        }
  },
  paths: {
  	  'datatables': 'plugins/DataTables/datatables.min',
      'datatables.net': 'plugins/DataTables/DataTables-1.10.18/js/jquery.dataTables.min',
      'datatables.net-bs4': 'plugins/DataTables/DataTables-1.10.18/js/dataTables.bootstrap4.min',

        //Dependencies
      'datatables.net-responsive': 'plugins/DataTables/Responsive-2.2.2/js/responsive.bootstrap4.min',
      'datatables.net-buttons' : 'plugins/DataTables/Buttons-1.5.2/js/dataTables.buttons.min',
      'datatables.net-buttons-bs': 'plugins/DataTables/Buttons-1.5.2/js/buttons.bootstrap4.min',
      'datatables.net-jszip': 'plugins/DataTables/JSZip-2.5.0/jszip.min',

      'datatables.net-html5': 'plugins/DataTables/Buttons-1.5.2/js/buttons.html5.min',
      'datatables.net-print': 'plugins/DataTables/Buttons-1.5.2/js/buttons.print.min',
      'datatables.net-pdfmake': 'plugins/DataTables/pdfmake-0.1.36/pdfmake.min',
      'datatables.net-vfs': 'plugins/DataTables/pdfmake-0.1.36/vfs_fonts',
      'datatables.net-colVis': 'plugins/DataTables/Buttons-1.5.2/js/buttons.colVis.min',
  }
});
